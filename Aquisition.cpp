#include "aquisition.h"
#include <cstring>
#include <fstream>
#include <stdio.h>
#include "util.h"

void Aquisition::incSig_x_carrReplic(std::complex<double> i_expCarr[EXT_PRN_CODE_LENGTH], std::complex<double> i_incSign[EXT_PRN_CODE_LENGTH*10], std::complex<double> o_mulResult[EXT_PRN_CODE_LENGTH*10])
{

    for (int row = 0; row <10; ++row)
    {
        for (int col = 0; col<EXT_PRN_CODE_LENGTH; ++col)
        {
            o_mulResult[col + row * EXT_PRN_CODE_LENGTH] = i_expCarr[col] * i_incSign[col + row * EXT_PRN_CODE_LENGTH];
        }
    }
}

void Aquisition::writeIncSig_x_carrReplic(char *i_fileName, std::complex<double> *i_vector, int i_size, int complexRepresentation)
{
    std::ofstream myfile;
    myfile.open (i_fileName, std::ios::out);

    char l_line[50], l_format[50];

    if (COMPLEX_REPRESENTATION)
        strcpy(l_format, "%.8lf %+.8lfi\n");
    else
        strcpy(l_format, "%.8lf %.8lf\n");


    for (int i = 0; i <i_size; ++i)
    {
        sprintf(l_line, l_format, i_vector[i].real(), i_vector[i].imag());
        myfile << l_line;
    }

    myfile << std::ends;
    myfile.close();
}

void Aquisition::correlation(complexVect *i_fftPrn, complexVect *i_fftReplica, complexVect *o_correlation, int i_size)
{
    for (int i = 0; i < i_size; ++i)
    {
       multFftw(i_fftPrn, i_fftReplica, o_correlation, i);
    }
}

void Aquisition::sumBuffer(long double *o_buffer, long double *i_buffer, int i_size, int i_offsetBuffer)
{
    for (int i = i_offsetBuffer; i < i_size + i_offsetBuffer; ++i)
    {
        o_buffer[i] += i_buffer[i - i_offsetBuffer];
    }
}


