#include "codePRNGenerator.h"
#include <cmath>
#include <iostream>
#include <stdio.h>
#include <fstream>


bool codePRNGenerator::m_pol1[10] = {1, 1, 1, 1, 1, 1, 1, 1, 1, 1};
bool codePRNGenerator::m_pol2[10] = {1, 1, 1, 1, 1, 1, 1, 1, 1, 1};


codePRNGenerator::codePRNGenerator()
{

}

bool codePRNGenerator::outPol1()
{
    return m_pol1[9];
}

bool codePRNGenerator::outPol2(int i_phaseSel1, int i_phaseSel2)
{
    int l_phaseSel1 = i_phaseSel1 - 1;
    int l_phaseSel2 = i_phaseSel2 - 1;
    return m_pol2[l_phaseSel1] XOR m_pol2[l_phaseSel2];
}

void codePRNGenerator::shiftRightPol(bool i_pol[])
{
    for (int i = 9; i > 0; --i)
    {
        i_pol[i] = i_pol[i-1];
    }
}

bool codePRNGenerator::outBufferBit(int l_sv)
{

    bool l_ret1 = outPol1();
    bool l_op1 = m_pol1[2] XOR m_pol1[9];
    shiftRightPol(m_pol1);
    m_pol1[0] = l_op1;

    int i_phaseSel1 = sv[l_sv][0];
    int i_phaseSel2 = sv[l_sv][1];
    bool l_ret2 = outPol2(i_phaseSel1, i_phaseSel2);
    bool l_op2 = m_pol2[1] XOR m_pol2[2] XOR m_pol2[5] XOR m_pol2[7] XOR m_pol2[8] XOR m_pol2[9];
    shiftRightPol(m_pol2);
    m_pol2[0] = l_op2;

    bool l_ret = l_ret1 XOR l_ret2;
    return l_ret;
}

void codePRNGenerator::resetPols()
{
    for (int i =0; i < 10; ++i)
    {
        m_pol1[i] = 1;
        m_pol2[i] = 1;
    }
}

void codePRNGenerator::generatePRN(char l_out[PRN_CODE_LENGTH], int l_sv)
{
    resetPols();

    for (int i = 0; i < 1023; ++i)
        l_out[i] = outBufferBit(l_sv)?-1:1;
}


void codePRNGenerator::generatePRN2(char i_prn[PRN_CODE_LENGTH], char l_out[EXT_PRN_CODE_LENGTH])
{
    const auto ts = 1/(12.5*1e6); //%settings.samplingFreq;
    const auto tc = 1/(1.023*1e6); //%settings.codeFreqBasis;
    const int samplesPerCode = EXT_PRN_CODE_LENGTH;
    int codeValueIndex[samplesPerCode];
    codeValueIndex[samplesPerCode-1] = PRN_CODE_LENGTH;

    for (int i =1; i<samplesPerCode; ++i)
    {
        codeValueIndex[i-1] = ceil((ts*i)/tc);
    }

    codeValueIndex[0] = 1;

    int l_outIndex = 0;

    for (int i = 0; i<PRN_CODE_LENGTH; ++i)
    {
        while (codeValueIndex[l_outIndex] - 1 == i)
        {
            l_out[l_outIndex] = i_prn[i];
            l_outIndex++;
        }
    }

}


void codePRNGenerator::writePrnToFile(char * i_fileName, char * i_vector, int i_size)
{
    std::ofstream myfile;
    myfile.open (i_fileName, std::ios::out);

    for (int i = 0; i <i_size; ++i)
    {
        myfile << (int)i_vector[i] << ',';
    }
    myfile << std::ends;
    myfile.close();
}
