#-------------------------------------------------
#
# Project created by QtCreator 2020-07-17T20:15:32
#
#-------------------------------------------------

QT       -= core

QT       -= gui

TARGET = testYasminGps
CONFIG   += console c++11
CONFIG   -= app_bundle

TEMPLATE = app


SOURCES += main.cpp \
    fft.cpp \
    codePRNGenerator.cpp \
    util.cpp \
    localReplicaCarrier.cpp \
    aquisition.cpp


HEADERS += \
    fft.h \
    codePRNGenerator.h \
    common.h \
    util.h \
    localReplicaCarrier.h \
    aquisition.h

LIBS += /usr/local/lib/libfftw3.a
