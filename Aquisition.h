#ifndef AQUISITION_H
#define AQUISITION_H

#include "common.h"

class Aquisition
{
public:
    static void incSig_x_carrReplic(std::complex<double> i_expCarr[EXT_PRN_CODE_LENGTH], std::complex<double> i_incSign[EXT_PRN_CODE_LENGTH*10],  std::complex<double> i_mulResult[EXT_PRN_CODE_LENGTH*10]);
    static void writeIncSig_x_carrReplic(char *i_fileName, std::complex<double>  *i_vector, int i_size, int complexRepresentation = NORMAL_REPRESENTATION);
    static void correlation (complexVect * i_fftPrn, complexVect *  i_fftReplica,complexVect * o_correlation, int i_size);
    static void sumBuffer(long double * o_buffer, long double * i_buffer, int i_size, int i_offsetBuffer);
};
#endif // AQUISITION_H
