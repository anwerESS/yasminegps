#include "util.h"
#include <fstream>

void constructComplexVect(char * in, complexVect * out, int i_size)
{
    for (int i = 0; i < i_size; ++i)
    {
        out[i][REAL] = in[i];
        out[i][IMAG] = 0;
    }
}



void stdComplexToFftw(std::complex<double> *in, complexVect *out, int i_size)
{
    for(int i = 0; i < i_size; ++i)
    {
        out[i][REAL] = in[i].real();
        out[i][IMAG] = in[i].imag();
    }
}


void fftwToStdComplex(complexVect *in, std::complex<double> * out, int i_size)
{
    for(int i = 0; i < i_size; ++i)
        out[i] = std::complex <double> (in[i][REAL], in[i][IMAG]);
}


/*  (R1 + j I1) (R2 + j I2)
*   = R1R2 + R1I2 j + I1R2 j - I1I2
*   = (R1R2 - I1I2) + i (R1I2 + I1R2)
*/
void multFftw(complexVect *n1, complexVect *n2, complexVect *res, int i_index)
{
    res[i_index][REAL] = ( n1[i_index][REAL] * n2[i_index][REAL] ) - ( n1[i_index][IMAG] * n2[i_index][IMAG]);
    res[i_index][IMAG] = ( n1[i_index][REAL] * n2[i_index][IMAG] ) + ( n1[i_index][IMAG] * n2[i_index][REAL]);
}


void fileTextToStdComplexArray(char *i_fileName, std::complex<double> *l_out)
{
    FILE * fp = fopen(i_fileName, "r");
    const int l_bufferLength = 50;
    char l_buffer[l_bufferLength];
    double re, im;

    if(fp != NULL)
    {
        for (int i = 0; i < EXT_PRN_CODE_LENGTH * 10; ++i)
        {
            fgets(l_buffer, l_bufferLength, fp);
            sscanf (l_buffer,"%lf %lf", &re, &im);
            l_out[i] = std::complex<double>(re, im);
        }
    }
    else
        printf("Eroor while open %s", i_fileName);

}


void fileTextToFftwComplexArray(char *i_fileName, complexVect *l_out)
{
    FILE * fp = fopen(i_fileName, "r");
    const int l_bufferLength = 50;
    char l_buffer[l_bufferLength];
    double re, im;

    for (int i = 0; i < EXT_PRN_CODE_LENGTH * 10; ++i)
    {
        fgets(l_buffer, l_bufferLength, fp);
        sscanf (l_buffer,"%lf %lf", &re, &im);
        l_out[i][REAL] = re;
        l_out[i][IMAG] = im;
    }
}


void stdComplexToFftw2(std::complex<double> *in, complexVect *out, int i_offset, int i_size)
{
    for(int i = i_offset; i < i_size + i_offset; ++i)
    {
        out[i - i_offset][REAL] = in[i].real();
        out[i - i_offset][IMAG] = in[i].imag();
    }
}


void normVector(complexVect *in, long double *out, int i_size)
{
    for (int i = 0; i < i_size; ++i)
    {
        out[i] = sqrt( (in[i][REAL] * in[i][REAL]) + (in[i][IMAG] * in[i][IMAG]) );
    }
}


void squareVector(long double *in, long double *out, int i_size)
{
    for (int i = 0; i < i_size; ++i)
    {
        out[i] = in[i] * in[i];
    }
}


void writeFileDouble(char * i_fileName, double *i_vector, int i_size, int i_mode)
{
    std::ofstream myfile;

    if (i_mode == FILE_WRIRE)
        myfile.open (i_fileName, std::ios::out);
    else if (i_mode == FILE_APPEND)
        myfile.open (i_fileName, std::ios::out | std::ios::app);

    char l_line[30];

    for (int i = 0; i <i_size; ++i)
    {
        sprintf(l_line, "%.8lf\n", i_vector[i]);
        myfile << l_line;
    }

    myfile << std::ends;
    myfile.close();
}

void writeFileDouble(char * i_fileName, long double *i_vector, int i_size, int i_mode)
{
    std::ofstream myfile;

    if (i_mode == FILE_WRIRE)
        myfile.open (i_fileName, std::ios::out);
    else if (i_mode == FILE_APPEND)
        myfile.open (i_fileName, std::ios::out | std::ios::app);

    char l_line[30];

    for (int i = 0; i <i_size; ++i)
    {
        sprintf(l_line, "%.8Lf\n", i_vector[i]);
        myfile << l_line;
    }

    myfile << std::ends;
    myfile.close();
}

void initializeVector(long double * o_vector, int i_size)
{
    for (int i = 0; i < i_size; ++i)
    {
        o_vector[i] = 0.0;
    }
}


void getMaxFromVector(long double *i_vector, long double  &o_maxValue, int &o_maxValueIndex, int i_size)
{
    o_maxValue = i_vector[0];
    o_maxValueIndex =0;

    for (int i = 0; i < i_size; ++i)
    {
        if (i_vector[i] > o_maxValue)
        {
            o_maxValue = i_vector[i];
            o_maxValueIndex = i;
        }
    }
}


void sumVector(long double *i_vector, long double &o_sumValue, int i_size)
{
    o_sumValue = 0;

    for (int i = 0; i < i_size; ++i)
    {
        o_sumValue += i_vector[i];
    }
}
