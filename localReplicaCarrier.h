#ifndef LOCALREPLICACARRIER_H
#define LOCALREPLICACARRIER_H

#include<complex>
#include<cmath>
#include "common.h"


class LocalReplicaCarrier
{
public:
    static void phasePoints_X_dopplerFreq(std::complex <double> * o_vect, int i_doppFreqIndex);
    static void expCarrier(std::complex <double>  *,  int i_doppFreqIndex);
    static void writeExpCarrier(char * i_fileName, std::complex <double> * i_vector, int i_size, int complexRepresentation = false);
};

#endif // LOCALREPLICACARRIER_H
