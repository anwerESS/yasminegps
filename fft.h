#ifndef FFT_H
#define FFT_H


#include <fftw3.h>
#include <iostream>
#include <cmath>

#include "common.h"

using namespace std;

class FFT{


public:
    static void fft(complexVect *in, complexVect *out, int i_size, int conjugated = COMPLEX_NOT_CONJUGATED);
    static void ifft(complexVect *in, complexVect *out, int i_size);
    static void conjugate(complexVect *out, int size);
    static void writeFFTFile(char * i_fileName, complexVect * i_vector, int i_size, int complexRepresentation = NORMAL_REPRESENTATION, int i_mode = FILE_WRIRE);
};






#endif // FFT_H
