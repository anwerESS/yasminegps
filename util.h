#ifndef UTIL_H
#define UTIL_H
#include "common.h"


void constructComplexVect(char * in, complexVect * out, int i_size);
void stdComplexToFftw(std::complex <double> * in, complexVect * out, int i_size);
void stdComplexToFftw2(std::complex <double> * in, complexVect * out, int i_offset, int i_size);
void fftwToStdComplex(complexVect * in, std::complex <double> * out, int i_size);
void multFftw(complexVect *n1, complexVect *n2, complexVect *res, int i_index);
void normVector(complexVect *in, long double * out, int i_size);
void squareVector(long double * in, long double * out, int i_size);
void initializeVector(long double * o_vector, int i_size);
void getMaxFromVector(long double * i_vector, long double &o_maxValue, int &o_maxValueIndex, int i_size);
void sumVector(long double * i_vector, long double & o_sumValue, int i_size);



void fileTextToStdComplexArray(char * i_fileName, std::complex<double> * l_out);
void fileTextToFftwComplexArray(char * i_fileName, complexVect * l_out);

void writeFileDouble(char * i_fileName, double * i_vector, int i_size, int i_mode = FILE_WRIRE);
void writeFileDouble(char * i_fileName, long double * i_vector, int i_size, int i_mode = FILE_WRIRE);



#endif // UTIL_H
