
#include "fft.h"
#include "common.h"
#include "util.h"
#include "codePRNGenerator.h"
#include "localReplicaCarrier.h"
#include "aquisition.h"




int main()
{      
    /* FileName */
    char l_fileName[80];

    /* generate PRN */
    char l_prnCode[PRN_CODE_LENGTH];
    codePRNGenerator::generatePRN(l_prnCode, codePRNGenerator::SV24);

    // generate ext PRN
    char l_extPrn[EXT_PRN_CODE_LENGTH];
    codePRNGenerator::generatePRN2(l_prnCode, l_extPrn);

    // input, output array
    complexVect l_inFFT[EXT_PRN_CODE_LENGTH], l_outFFt[EXT_PRN_CODE_LENGTH];

    constructComplexVect(l_extPrn, l_inFFT, EXT_PRN_CODE_LENGTH); // char(real) --> complexVect(real, im=0)
    // compute FFT
    FFT::fft(l_inFFT, l_outFFt, EXT_PRN_CODE_LENGTH, COMPLEX_CONJUGATED);

    /* calc incomming signal */
    std::complex<double> * l_incSign = new std::complex<double>[INCOMMING_SIGN_LENGH];
    sprintf(l_fileName, "%s%s", FILEANME_DIRECTORY, "incommingSignal2.txt");
    fileTextToStdComplexArray(l_fileName, l_incSign);

    /* convert l_mulIncSign_x_l_replCarr (std::complex<float> ==> complexVect) to compute fft*/
    complexVect * l_incSign_x_carrReplicaFftw = new complexVect [BUFFER_LENGTH];

    /* allocate memory & delc Arrays*/
    std::complex<double> l_replCarr[12500];
    std::complex<double> * l_mulIncSign_x_l_replCarr =  new std::complex<double>[INCOMMING_SIGN_LENGH];

    complexVect * l_outFftw = new complexVect[BUFFER_LENGTH];
    complexVect * l_correlation = new complexVect[BUFFER_LENGTH];
    complexVect * l_Ifftcorrelation = new complexVect[BUFFER_LENGTH];
    long double normIfft[BUFFER_LENGTH];
    long double squareIfft[BUFFER_LENGTH];
    long double * l_aquisition = new long double[BUFFER_LENGTH_x_BUFFER_FREQ_NBR];

    /* init l_aquisition vector */
    initializeVector(l_aquisition, BUFFER_LENGTH_x_BUFFER_FREQ_NBR);

    int l_offset =0;

    for (int l_doplperIndex = 0; l_doplperIndex < BUFFER_FREQ_NBR; ++l_doplperIndex)
    {
        /* calc exp carrier */
        LocalReplicaCarrier::expCarrier(l_replCarr, l_doplperIndex);

        /* incomming signal and replica multiplication*/
        Aquisition::incSig_x_carrReplic(l_replCarr, l_incSign, l_mulIncSign_x_l_replCarr);

        l_offset =0;
        for (int bufferIndex = 0; bufferIndex < BUFFER_NBR; ++bufferIndex)
        {
            l_offset = BUFFER_LENGTH * bufferIndex;
            stdComplexToFftw2(l_mulIncSign_x_l_replCarr, l_incSign_x_carrReplicaFftw, l_offset, BUFFER_LENGTH);

            /* fft */
            FFT::fft(l_incSign_x_carrReplicaFftw, l_outFftw, BUFFER_LENGTH);

            /* corelation PRN & LocalRepilica */
            FFT::conjugate(l_outFftw, BUFFER_LENGTH);
            Aquisition::correlation(l_outFFt, l_outFftw, l_correlation, BUFFER_LENGTH);

            /* IFFT */
            FFT::ifft(l_correlation, l_Ifftcorrelation, BUFFER_LENGTH);

            /* Norm */
            normVector(l_Ifftcorrelation, normIfft, BUFFER_LENGTH);

            /* sqaure */
            squareVector(normIfft, squareIfft, BUFFER_LENGTH);

            /* sum buffer */
            Aquisition::sumBuffer(l_aquisition, squareIfft, BUFFER_LENGTH, BUFFER_LENGTH * l_doplperIndex);
        }
    }

    /* get l_aquisition max value */
    long double l_maxAquisition;
    int l_maxAquisitionIndex;
    getMaxFromVector(l_aquisition, l_maxAquisition, l_maxAquisitionIndex, BUFFER_LENGTH_x_BUFFER_FREQ_NBR);
    std::cout << "MAX  = " << l_maxAquisition << '\n';

    long double l_sumAquisition = 0;
    sumVector(l_aquisition, l_sumAquisition, BUFFER_LENGTH_x_BUFFER_FREQ_NBR);
    std::cout << "SUM  = " << l_sumAquisition << '\n';

    //////////////////////////////////////////////////////////////

    /* free memory */
    delete [] l_incSign;
    delete [] l_mulIncSign_x_l_replCarr;

    delete [] l_incSign_x_carrReplicaFftw;
    delete [] l_outFftw;
    delete [] l_correlation;
    delete [] l_Ifftcorrelation;

    //////////////////////////// END //////////////////////////////

    /* do not forget to delete memory */
    return 0;
}

